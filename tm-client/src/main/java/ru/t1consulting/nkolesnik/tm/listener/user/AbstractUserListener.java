package ru.t1consulting.nkolesnik.tm.listener.user;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1consulting.nkolesnik.tm.api.endpoint.IAuthEndpoint;
import ru.t1consulting.nkolesnik.tm.api.endpoint.IUserEndpoint;
import ru.t1consulting.nkolesnik.tm.dto.model.UserDTO;
import ru.t1consulting.nkolesnik.tm.listener.AbstractListener;

@Getter
@Component
public abstract class AbstractUserListener extends AbstractListener {

    @NotNull
    @Autowired
    public IUserEndpoint userEndpoint;

    @NotNull
    @Autowired
    public IAuthEndpoint authEndpoint;

    protected void showUser(@NotNull final UserDTO user) {
        System.out.println("ID: " + user.getId());
        System.out.println("FIRST NAME: " + user.getFirstName());
        System.out.println("MIDDLE NAME: " + user.getMiddleName());
        System.out.println("LAST NAME: " + user.getLastName());
        System.out.println("EMAIL: " + user.getEmail());
        System.out.println("ROLE: " + user.getRole());
    }

}
