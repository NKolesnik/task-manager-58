package ru.t1consulting.nkolesnik.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1consulting.nkolesnik.tm.dto.model.TaskDTO;
import ru.t1consulting.nkolesnik.tm.dto.request.task.TaskStartByIdRequest;
import ru.t1consulting.nkolesnik.tm.dto.response.task.TaskStartByIdResponse;
import ru.t1consulting.nkolesnik.tm.event.ConsoleEvent;
import ru.t1consulting.nkolesnik.tm.exception.entity.TaskNotFoundException;
import ru.t1consulting.nkolesnik.tm.util.TerminalUtil;

@Component
public final class TaskStartByIdListener extends AbstractTaskListener {

    @NotNull
    public static final String NAME = "task-start-by-id";

    @NotNull
    public static final String DESCRIPTION = "Start task by id.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @EventListener(condition = "@taskStartByIdListener.getName() == #event.name")
    public void handler(@NotNull ConsoleEvent event) {
        System.out.println("[START TASK BY ID]");
        System.out.println("[ENTER ID:]");
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull final TaskStartByIdRequest request = new TaskStartByIdRequest(getToken());
        request.setId(id);
        @NotNull final TaskStartByIdResponse response = getTaskEndpoint().startTaskById(request);
        @Nullable TaskDTO task = response.getTask();
        if (task == null) throw new TaskNotFoundException();
        showTask(task);
    }

}
