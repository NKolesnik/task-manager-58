package ru.t1consulting.nkolesnik.tm.listener.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1consulting.nkolesnik.tm.dto.request.data.DataXmlJaxbLoadRequest;
import ru.t1consulting.nkolesnik.tm.enumerated.Role;
import ru.t1consulting.nkolesnik.tm.event.ConsoleEvent;

@Component
public final class DataXmlJaxbLoadListener extends AbstractDataListener {

    @NotNull
    public static final String NAME = "data-load-jaxb-xml";

    @NotNull
    public static final String DESCRIPTION = "Load data from xml file using JAXB.";

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    @NotNull
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @SneakyThrows
    @EventListener(condition = "@dataXmlJaxbLoadListener.getName() == #event.name")
    public void handler(@NotNull ConsoleEvent event) {
        getDomainEndpoint().loadDataXmlJaxb(new DataXmlJaxbLoadRequest(getToken()));
    }

    @Override
    @NotNull
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
