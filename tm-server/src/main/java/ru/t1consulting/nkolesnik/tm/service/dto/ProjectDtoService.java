package ru.t1consulting.nkolesnik.tm.service.dto;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import ru.t1consulting.nkolesnik.tm.api.repository.dto.IProjectRepositoryDTO;
import ru.t1consulting.nkolesnik.tm.api.service.dto.IProjectDtoService;
import ru.t1consulting.nkolesnik.tm.dto.model.ProjectDTO;
import ru.t1consulting.nkolesnik.tm.enumerated.Sort;
import ru.t1consulting.nkolesnik.tm.enumerated.Status;
import ru.t1consulting.nkolesnik.tm.exception.entity.ProjectNotFoundException;
import ru.t1consulting.nkolesnik.tm.exception.entity.StatusNotFoundException;
import ru.t1consulting.nkolesnik.tm.exception.field.DescriptionEmptyException;
import ru.t1consulting.nkolesnik.tm.exception.field.NameEmptyException;
import ru.t1consulting.nkolesnik.tm.exception.field.ProjectIdEmptyException;
import ru.t1consulting.nkolesnik.tm.exception.field.UserIdEmptyException;

import javax.persistence.EntityManager;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

@Service
public final class ProjectDtoService extends AbstractUserOwnedDtoService<ProjectDTO, IProjectRepositoryDTO> implements IProjectDtoService {

    @NotNull
    @Override
    protected IProjectRepositoryDTO getRepository() {
        return context.getBean(IProjectRepositoryDTO.class);
    }

    @Override
    public void add(@Nullable final ProjectDTO project) {
        if (project == null) throw new ProjectNotFoundException();
        @NotNull final IProjectRepositoryDTO repository = getRepository();
        @NotNull final EntityManager em = repository.getEntityManager();
        try {
            em.getTransaction().begin();
            repository.add(project);
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
            throw e;
        } finally {
            em.close();
        }
    }

    @Override
    public void add(@Nullable final String userId, @Nullable final ProjectDTO project) {
        if (project == null) throw new ProjectNotFoundException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final IProjectRepositoryDTO repository = getRepository();
        @NotNull final EntityManager em = repository.getEntityManager();
        try {
            em.getTransaction().begin();
            repository.add(userId, project);
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
            throw e;
        } finally {
            em.close();
        }
    }

    @Override
    public void add(@Nullable final Collection<ProjectDTO> projects) {
        if (projects == null || projects.isEmpty()) throw new ProjectNotFoundException();
        @NotNull final IProjectRepositoryDTO repository = getRepository();
        @NotNull final EntityManager em = repository.getEntityManager();
        try {
            em.getTransaction().begin();
            for (ProjectDTO project : projects)
                repository.add(project);
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
            throw e;
        } finally {
            em.close();
        }
    }

    @Override
    public void add(@Nullable final String userId, @Nullable final Collection<ProjectDTO> projects) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projects == null || projects.isEmpty()) throw new ProjectNotFoundException();
        @NotNull final IProjectRepositoryDTO repository = getRepository();
        @NotNull final EntityManager em = repository.getEntityManager();
        try {
            em.getTransaction().begin();
            for (ProjectDTO project : projects)
                repository.add(userId, project);
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
            throw e;
        } finally {
            em.close();
        }
    }

    @Override
    public void set(@Nullable final Collection<ProjectDTO> projects) {
        if (projects == null || projects.isEmpty()) throw new ProjectNotFoundException();
        @NotNull final IProjectRepositoryDTO repository = getRepository();
        @NotNull final EntityManager em = repository.getEntityManager();
        try {
            em.getTransaction().begin();
            repository.clear();
            for (ProjectDTO project : projects)
                repository.add(project);
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
            throw e;
        } finally {
            em.close();
        }
    }

    @Override
    public void set(@Nullable final String userId, @Nullable final Collection<ProjectDTO> projects) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projects == null || projects.isEmpty()) throw new ProjectNotFoundException();
        @NotNull final IProjectRepositoryDTO repository = getRepository();
        @NotNull final EntityManager em = repository.getEntityManager();
        try {
            em.getTransaction().begin();
            repository.clear();
            for (ProjectDTO project : projects)
                repository.add(userId, project);
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
            throw e;
        } finally {
            em.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectDTO create(
            @Nullable final String userId,
            @Nullable final String name
    ) {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final ProjectDTO project = new ProjectDTO();
        @NotNull final IProjectRepositoryDTO repository = getRepository();
        @NotNull final EntityManager em = repository.getEntityManager();
        try {
            em.getTransaction().begin();
            project.setName(name);
            project.setUserId(userId);
            repository.add(project);
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
            throw e;
        } finally {
            em.close();
        }
        return project;
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectDTO create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @NotNull final ProjectDTO project = new ProjectDTO();
        @NotNull final IProjectRepositoryDTO repository = getRepository();
        @NotNull final EntityManager em = repository.getEntityManager();
        try {
            em.getTransaction().begin();
            project.setName(name);
            project.setUserId(userId);
            project.setDescription(description);
            repository.add(project);
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
            throw e;
        } finally {
            em.close();
        }
        return project;
    }

    @Override
    public long getSize() {
        @NotNull final IProjectRepositoryDTO repository = getRepository();
        @NotNull final EntityManager em = repository.getEntityManager();
        try {
            return repository.getSize();
        } finally {
            em.close();
        }
    }

    @Override
    public long getSize(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final IProjectRepositoryDTO repository = getRepository();
        @NotNull final EntityManager em = repository.getEntityManager();
        try {
            return repository.getSize(userId);
        } finally {
            em.close();
        }

    }

    @NotNull
    @Override
    public List<ProjectDTO> findAll() {
        @NotNull final IProjectRepositoryDTO repository = getRepository();
        @NotNull final EntityManager em = repository.getEntityManager();
        try {
            @Nullable final List<ProjectDTO> projects;
            projects = repository.findAll();
            if (projects.isEmpty()) return Collections.emptyList();
            return projects;
        } finally {
            em.close();
        }
    }

    @NotNull
    @Override
    public List<ProjectDTO> findAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @Nullable final List<ProjectDTO> projects;
        @NotNull final IProjectRepositoryDTO repository = getRepository();
        @NotNull final EntityManager em = repository.getEntityManager();
        try {
            projects = repository.findAll(userId);
            if (projects.isEmpty()) return Collections.emptyList();
            return projects;
        } finally {
            em.close();
        }
    }

    @NotNull
    @Override
    public List<ProjectDTO> findAll(@Nullable final Comparator<ProjectDTO> comparator) {
        if (comparator == null) return findAll();
        @Nullable final List<ProjectDTO> projectList;
        @NotNull final IProjectRepositoryDTO repository = getRepository();
        @NotNull final EntityManager em = repository.getEntityManager();
        try {
            em.getTransaction().begin();
            projectList = repository.findAll(comparator);
            if (projectList.isEmpty()) return Collections.emptyList();
            return projectList;
        } finally {
            em.close();
        }
    }

    @NotNull
    @Override
    public List<ProjectDTO> findAll(@Nullable final Sort sort) {
        if (sort == null) return findAll();
        @Nullable final List<ProjectDTO> projectList;
        @NotNull final IProjectRepositoryDTO repository = getRepository();
        @NotNull final EntityManager em = repository.getEntityManager();
        try {
            projectList = repository.findAll(sort);
            if (projectList.isEmpty()) return Collections.emptyList();
            return projectList;
        } finally {
            em.close();
        }
    }

    @NotNull
    @Override
    public List<ProjectDTO> findAll(@Nullable final String userId, @Nullable final Comparator<ProjectDTO> comparator) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (comparator == null) return findAll();
        @Nullable final List<ProjectDTO> projectList;
        @NotNull final IProjectRepositoryDTO repository = getRepository();
        @NotNull final EntityManager em = repository.getEntityManager();
        try {
            projectList = repository.findAll(userId, comparator);
            if (projectList.isEmpty()) return Collections.emptyList();
            return projectList;
        } finally {
            em.close();
        }
    }

    @NotNull
    @Override
    public List<ProjectDTO> findAll(@Nullable final String userId, @Nullable final Sort sort) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (sort == null) return findAll(userId);
        @Nullable final List<ProjectDTO> projectList;
        @NotNull final IProjectRepositoryDTO repository = getRepository();
        @NotNull final EntityManager em = repository.getEntityManager();
        try {
            projectList = repository.findAll(userId, sort);
            if (projectList.isEmpty()) return Collections.emptyList();
            return projectList;
        } finally {
            em.close();
        }
    }

    @Nullable
    @Override
    public ProjectDTO findById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new ProjectIdEmptyException();
        @NotNull final IProjectRepositoryDTO repository = getRepository();
        @NotNull final EntityManager em = repository.getEntityManager();
        try {
            return repository.findById(id);
        } finally {
            em.close();
        }
    }

    @Nullable
    @Override
    public ProjectDTO findById(@Nullable final String userId, @Nullable final String id) {
        if (id == null || id.isEmpty()) throw new ProjectIdEmptyException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final IProjectRepositoryDTO repository = getRepository();
        @NotNull final EntityManager em = repository.getEntityManager();
        try {
            return repository.findById(userId, id);
        } finally {
            em.close();
        }
    }

    @Override
    public boolean existsById(@Nullable final String id) {
        if (id == null || id.isEmpty()) return false;
        @NotNull final IProjectRepositoryDTO repository = getRepository();
        @NotNull final EntityManager em = repository.getEntityManager();
        try {
            return repository.existsById(id);
        } finally {
            em.close();
        }
    }

    @Override
    public boolean existsById(@Nullable final String userId, @Nullable final String id) {
        if (id == null || id.isEmpty()) return false;
        if (userId == null || userId.isEmpty()) return false;
        @NotNull final IProjectRepositoryDTO repository = getRepository();
        @NotNull final EntityManager em = repository.getEntityManager();
        try {
            return repository.existsById(userId, id);
        } finally {
            em.close();
        }
    }

    @Override
    public void update(@Nullable final ProjectDTO project) {
        if (project == null) throw new ProjectNotFoundException();
        @NotNull final IProjectRepositoryDTO repository = getRepository();
        @NotNull final EntityManager em = repository.getEntityManager();
        try {
            em.getTransaction().begin();
            repository.update(project);
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
            throw e;
        } finally {
            em.close();
        }
    }

    @Override
    @SneakyThrows
    public void updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (id == null || id.isEmpty()) throw new ProjectIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @NotNull final IProjectRepositoryDTO repository = getRepository();
        @NotNull final EntityManager em = repository.getEntityManager();
        try {
            em.getTransaction().begin();
            @Nullable final ProjectDTO project = repository.findById(userId, id);
            if (project == null) throw new ProjectNotFoundException();
            project.setName(name);
            project.setDescription(description);
            repository.update(project);
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
            throw e;
        } finally {
            em.close();
        }
    }

    @Override
    @SneakyThrows
    public void changeProjectStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) {
        if (status == null) throw new StatusNotFoundException();
        if (id == null || id.isEmpty()) throw new ProjectIdEmptyException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final IProjectRepositoryDTO repository = getRepository();
        @NotNull final EntityManager em = repository.getEntityManager();
        try {
            em.getTransaction().begin();
            @Nullable final ProjectDTO project = repository.findById(userId, id);
            if (project == null) throw new ProjectNotFoundException();
            project.setStatus(status);
            repository.update(project);
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
            throw e;
        } finally {
            em.close();
        }
    }

    @Override
    public void clear() {
        @NotNull final IProjectRepositoryDTO repository = getRepository();
        @NotNull final EntityManager em = repository.getEntityManager();
        try {
            em.getTransaction().begin();
            repository.clear();
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
            throw e;
        } finally {
            em.close();
        }
    }

    @Override
    public void clear(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final IProjectRepositoryDTO repository = getRepository();
        @NotNull final EntityManager em = repository.getEntityManager();
        try {
            em.getTransaction().begin();
            repository.clear(userId);
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
            throw e;
        } finally {
            em.close();
        }
    }

    @Override
    public void remove(@Nullable final ProjectDTO project) {
        if (project == null) throw new ProjectNotFoundException();
        @NotNull final IProjectRepositoryDTO repository = getRepository();
        @NotNull final EntityManager em = repository.getEntityManager();
        try {
            em.getTransaction().begin();
            repository.remove(project);
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
            throw e;
        } finally {
            em.close();
        }
    }

    @Override
    public void remove(@Nullable final String userId, @Nullable final ProjectDTO project) {
        if (project == null) throw new ProjectNotFoundException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final IProjectRepositoryDTO repository = getRepository();
        @NotNull final EntityManager em = repository.getEntityManager();
        try {
            em.getTransaction().begin();
            repository.remove(userId, project);
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
            throw e;
        } finally {
            em.close();
        }
    }

    @Override
    public void removeById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new ProjectIdEmptyException();
        @NotNull final IProjectRepositoryDTO repository = getRepository();
        @NotNull final EntityManager em = repository.getEntityManager();
        try {
            em.getTransaction().begin();
            repository.removeById(id);
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
            throw e;
        } finally {
            em.close();
        }
    }

    @Override
    public void removeById(@Nullable final String userId, @Nullable final String id) {
        if (id == null || id.isEmpty()) throw new ProjectIdEmptyException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final IProjectRepositoryDTO repository = getRepository();
        @NotNull final EntityManager em = repository.getEntityManager();
        try {
            em.getTransaction().begin();
            repository.removeById(userId, id);
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
            throw e;
        } finally {
            em.close();
        }
    }

}
