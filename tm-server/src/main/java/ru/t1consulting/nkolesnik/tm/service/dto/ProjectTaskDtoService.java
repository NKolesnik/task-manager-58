package ru.t1consulting.nkolesnik.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.t1consulting.nkolesnik.tm.api.service.IProjectTaskService;
import ru.t1consulting.nkolesnik.tm.api.service.dto.IProjectDtoService;
import ru.t1consulting.nkolesnik.tm.api.service.dto.ITaskDtoService;
import ru.t1consulting.nkolesnik.tm.dto.model.TaskDTO;
import ru.t1consulting.nkolesnik.tm.exception.entity.ProjectNotFoundException;
import ru.t1consulting.nkolesnik.tm.exception.entity.TaskNotFoundException;
import ru.t1consulting.nkolesnik.tm.exception.entity.UserNotFoundException;
import ru.t1consulting.nkolesnik.tm.exception.field.ProjectIdEmptyException;
import ru.t1consulting.nkolesnik.tm.exception.field.TaskIdEmptyException;
import ru.t1consulting.nkolesnik.tm.exception.field.UserIdEmptyException;

import java.util.List;
import java.util.Optional;

@Service
public final class ProjectTaskDtoService implements IProjectTaskService {

    @NotNull
    @Autowired
    private IProjectDtoService projectService;

    @NotNull
    @Autowired
    private ITaskDtoService taskService;


    @Override
    public void bindTaskToProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) {
        Optional.ofNullable(userId).orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(projectId).orElseThrow(ProjectIdEmptyException::new);
        Optional.ofNullable(taskId).orElseThrow(TaskIdEmptyException::new);
        if (!projectService.existsById(projectId)) return;
        @NotNull final TaskDTO task = Optional.ofNullable(taskService.findById(userId, taskId))
                .orElseThrow(TaskNotFoundException::new);
        task.setProjectId(projectId);
        taskService.update(task);
    }

    @Override
    public void unbindTaskFromProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) {
        Optional.ofNullable(userId).orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(projectId).orElseThrow(ProjectIdEmptyException::new);
        Optional.ofNullable(taskId).orElseThrow(TaskIdEmptyException::new);
        if (!projectService.existsById(projectId)) throw new ProjectNotFoundException();
        @NotNull final TaskDTO task = Optional.ofNullable(taskService.findById(userId, taskId))
                .orElseThrow(TaskNotFoundException::new);
        task.setProjectId(null);
        taskService.update(task);
    }

    @Override
    public void removeProjectById(@Nullable final String userId, @Nullable final String projectId) {
        Optional.ofNullable(userId).orElseThrow(UserNotFoundException::new);
        Optional.ofNullable(projectId).orElseThrow(ProjectIdEmptyException::new);
        @Nullable final List<TaskDTO> tasks = taskService.findAllByProjectId(userId, projectId);
        tasks.forEach(task -> taskService.removeById(userId, task.getId()));
        projectService.removeById(projectId);
    }

}
