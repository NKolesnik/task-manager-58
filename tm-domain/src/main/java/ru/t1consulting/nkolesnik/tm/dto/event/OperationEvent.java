package ru.t1consulting.nkolesnik.tm.dto.event;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.t1consulting.nkolesnik.tm.enumerated.EntityOperationType;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public class OperationEvent {

    @NotNull
    @JsonFormat(shape = JsonFormat.Shape.STRING,
            pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSXXX")
    private final Date date = new Date();
    @NotNull
    private EntityOperationType operationType;
    @NotNull
    private Object entity;
    @NotNull
    private String table;

    public OperationEvent(
            @NotNull final EntityOperationType operationType,
            @NotNull final Object entity
    ) {
        this.operationType = operationType;
        this.entity = entity;
    }

}